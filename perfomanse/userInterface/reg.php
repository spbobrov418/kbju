<?php

session_start();


?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/skeleton/normalize.css" rel="stylesheet">
        <link href="../css/skeleton/skeleton.css" rel="stylesheet">
        <title>Регистрация</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                
                <div class="five columns">
                    
                    <h3>Регистрация</h3>

                    <form action="../../logic/userInterface/saveUser.php" method="post">
                        <p>
                            <label>Логин:</label>
                            <input name="userName" type="text" size="15" maxlength="15">
                        </p>
                        <p>
                            <label>Пароль:</label>
                            <input name="userPassword" type="password" size="15" maxlength="15">
                        </p>
                        <p>
                            <label>E-mail:</label>
                            <input name="userEmail" type="email" size="15" maxlength="30">
                        </p>
                        <p>
                            <input type="submit" name="submit" value="Регистрация">
                        </p>

                    </form>
                    
                </div>
                
                <div class="four columns">
                    
                    <h3>Начало</h3>
                    
                    <ul class="nav">

                        <li><a href="../../index.php">Главная</a></li>
                        
                        <li><a href="writeMe.php">Написать разработчику</a></li>

                    </ul>
                    
                </div>
                
            </div>
            
                <div class="row">
                    
                    <div class="five columns">
                        
                        <?php


                            if(empty($_SESSION['valid user']))
                            {
                                echo "Вы вошли на сайт как гость <br/>";
                                if (!empty($_SESSION['msg user']))
                                    {
                                        echo ($_SESSION['msg user']);
                                        $_SESSION['msg user'] = '';
                                    }
                            
                            }
                            else 
                            {
                                echo "Вы вошли как ".$_SESSION['valid user']."<br/>";
                                if (!empty($_SESSION['msg user']))
                                    {
                                        echo ($_SESSION['msg user']);
                                        $_SESSION['msg user'] = '';
                                    }
                            }
                            
                        ?>
                </div>
        </div>
        </div>
        
    </body>
</html>
