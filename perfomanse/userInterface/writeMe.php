<?php
session_start();


?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/skeleton/normalize.css" rel="stylesheet">
        <link href="../css/skeleton/skeleton.css" rel="stylesheet">
        <title>Пишите!</title>
    </head>
    
    <body>
        <div class="container">
            <div class="row">
                <div class="five columns">
                    
                    <h3>Написать разработчику</h3>

                    <form action="../../logic/userInterface/writeMeProcess.php" method="post">
                        <textarea cols="30" name="msg" placeholder="Ваше сообщение:"></textarea>
                        <br/>
                        <button id="send">Отправить</button>
                    </form>
        </div>
                
                <div class="four columns">
                    
                    <h3>Начало</h3>
                    <ul class="nav">

                        <li><a href="reg.php">Регистрация</a></li>
                        <li><a href="logout.php">Выход</a></li>
                        

                    </ul>
                    
                </div>
        
               <div class="three columns">
                    
                    <h3>Пользователям</h3>
                    <ul class="nav">

                        <li><a href="../kbjuProcess/toEat.php">Прием пищи</a></li>
                        <li><a href="../kbjuProcess/newFood.php">Новый продукт</a></li>
                        <li><a href="../statistic/statistic.php">Статистика</a></li>

                    </ul>
                    
                </div>
            </div>
            <div class="row">
                <div class="five columns">
        <?php
        
                
            if(empty($_SESSION['valid user']))
            {
                echo "Вы вошли на сайт как гость <br/>";
            }
            else 
            {
                echo "Вы вошли как ".$_SESSION['valid user']."<br/>";
            }
            if(!empty($_SESSION['writeme']))
            {
                echo ($_SESSION['writeme']);
                unset($_SESSION['writeme']); //убиваем переменную
            }
        ?>
                </div>
        </div>
        </div>
    </body>
</html>
