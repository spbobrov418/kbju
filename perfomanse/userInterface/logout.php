<?php

session_start();

if(!empty($_SESSION['valid user']))
{
    $oldUser = $_SESSION['valid user'];
    unset($_SESSION['valid user']);//Удаляет переменную
    session_destroy();//Destroys all data registered to a session
}

?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/skeleton/normalize.css" rel="stylesheet">
        <link href="../css/skeleton/skeleton.css" rel="stylesheet">
        <title>Главная</title>
    </head>

    <body>
        <div class="container">
            <div class="row">
        
       
        
                <div class="four columns">

                <h3>Удачи во всем</h3>

                <ul class="nav">

                    <li><a href="reg.php">Регистрация</a></li>

                    <li><a href="writeMe.php">Написать разработчику</a></li>
                    <li><a href="../../index.php">На главную</a></li>

                </ul>

                </div>
            </div>
            <div class="row">
                    <div class="five columns">
                         <?php
                        if (!empty($oldUser))
                        {
                        echo 'Успешный выход';
                        }
                        else
                        {
                        echo 'Вы не входили в систему <br/>';
                        }
                        ?>
                    </div>
            </div>
        </div>
    </body>
</html>

