<?php
        session_start();
?>

<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/skeleton/normalize.css" rel="stylesheet">
        <link href="../css/skeleton/skeleton.css" rel="stylesheet">
        <title>Главная</title>
    </head>
    <body>
        <div class="container">
            
            <div class="row">
                
                <div class="five columns">
                    
                    <h3>Статистика</h3>

                    <?php
                        
                        echo "Период ".$_SESSION['dateForStatistic']." <br/>";
                        echo "<br/>";
                        echo "Калории ".$_SESSION['cal']." <br/>";
                        echo "Белки ".$_SESSION['protein']." <br/>";
                        echo "Жиры ".$_SESSION['fat']." <br/>";
                        echo "Углеводы ".$_SESSION['carbo']." <br/>";
                        
                        if(strlen($_SESSION['dateForStatistic'])>10)
                        {
                            echo "<br/>";
                            echo "Среднесуточное <br/>";
                            echo "<br/>";
                             
                            echo "Калории ".$_SESSION['cal']/$_SESSION['countData']." <br/>";
                            echo "Белки ".$_SESSION['protein']/$_SESSION['countData']." <br/>";
                            echo "Жиры ".$_SESSION['fat']/$_SESSION['countData']." <br/>";
                            echo "Углеводы ".$_SESSION['carbo']/$_SESSION['countData']." <br/>";
                        }
                        
                    ?>
                </div>
            
                <div class="four columns">
                    
                    <h3>Сегодня</h3>

                    <?php

                        $userName = "Guest"; // для неавторизованного пользователя (напр. написать отзыв)
                        $userID = 617; //номер для гостевого пользователя
                        $dateForStatistic = date("Y-m-d");//для отображения данных на текущую дату

                        //require '/var/www/u0860180/data/dbOn.php';        
                          require 'dbOn.php';    
                        
                        if (mysqli_connect_errno())
                        {
                            echo 'Не удалось установить соединение';
                            exit;
                        }

                        $query = "select * from user79 where userName like '%".$userName."%'";//выбрать из базы пользователей по введеному логину
                        $result = $db->query($query);

                        $row = $result->fetch_assoc();//Возвращает ассоциативный массив с названиями индексов, соответсвующими названиям колонок
                        $userID = ($row['userID']); //выбираю значение с индексом userID 


                        
                        $query = "select * from toeat where userID='".$userID."' and toEatData='".$dateForStatistic."'";//формируем запрос к базе по двум критериям
                        $result = $db->query($query);//выборка из базы
                        $num_results = $result->num_rows;//количество выбранных строк


                        $cal = 0;//задаем начальные значения
                        $protein = 0;
                        $fat = 0;
                        $carbo = 0;


                        for ($i = 0; $i < $num_results; $i++)//в цикле перебираем все строки выборки, суммируем данные
                        {
                            $row = $result->fetch_assoc();
                            $cal = $cal + $row['cal'];
                            $protein = $protein + $row['protein'];
                            $fat = $fat + $row['fat'];
                            $carbo = $carbo + $row['carbo'];

                        }
                                
                                
                                echo "Калории ".$cal;
                                echo '</br>';
                                echo "Белки ".$protein;
                                echo '</br>';
                                echo "Жиры ".$fat;
                                echo '</br>';
                                echo "Углеводы ".$carbo;
                                echo '</br>';

                                $result->free();//Освобождает память от результата запроса
                                $db->close();
                    ?>
                </div>
                
                <div class="three columns">

                    <h3>Пользователям</h3>

                    <ul class="nav">

                        <li><a href="../../index.php">Главная</a></li>
                        <li><a href="../kbjuProcess/toEat.php">Прием пищи</a></li>
                        <li><a href="../kbjuProcess/newFood.php">Новый продукт</a></li>
                        <li><a href="statistic.php">Статистика</a></li>
                        <li><a href="../userInterface/logout.php">Выход</a></li>
                        <li><a href="../userInterface/reg.php">Регистрация</a></li>
                        <li><a href="../userInterface/writeMe.php">Написать разработчику</a></li>


                    </ul>

                </div>
            </div>
            <div class="row">
                <div class="five columns">
                    
                    <?php


                        if(empty($_SESSION['valid user']))
                        {
                            echo "Вы вошли на сайт как гость";
                        }
                        else 
                        {
                            echo "<br/>";
                            echo "Вы вошли как ".$_SESSION['valid user'];
                        }
                    ?>

                </div>
            </div>
        </div>


        
    </body>
</html>

