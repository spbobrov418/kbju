<?php
session_start();

?>

<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/skeleton/normalize.css" rel="stylesheet">
        <link href="../css/skeleton/skeleton.css" rel="stylesheet">
        <title>Новый продукт</title>
    </head>
    <body>
        <div class="container">
            
            <div class="row">
                
                <div class="five columns">
                    
                    <h3>Новый продукт</h3>
                    
                    <form action="../../logic/kbjuProcess/foodArr.php" method="post">
                        
                    <p>
                        <label>Название</label>
                        <input type="text" name="nameFood">
                    </p>
                    
                    <p>
                        <label>Калорийность</label>
                        <input type="text" name="cal" value="0">
                    </p>
                    
                    <p>
                        <label>Белков</label>
                        <input type="text" name="protein" value="0">
                    </p>
                    
                    <p>
                        <label>Жиров</label>
                        <input type="text" name="fat" value="0">
                    </p>
                    
                    <p>
                        <label>Углеводов:</label>
                        <input type="text" name="carbo" value="0">
                    </p>
                    
                    <input type="submit">
                    </form>
                </div>
                
                <div class="four columns">
                    
                    <h3>Пользователям</h3>
                    <ul class="nav">

                        <li><a href="../../index.php">Главная</a></li>
                        <li><a href="toEat.php">Прием пищи</a></li>
                        <li><a href="../statistic/statistic.php">Статистика</a></li>
                        <li><a href="../userInterface/logout.php">Выход</a></li>
                        <li><a href="../userInterface/reg.php">Регистрация</a></li>
                        <li><a href="../userInterface/writeMe.php">Написать разработчику</a></li>

                    </ul>
                    
                </div>
                    
            </div>  
            <div class="row">
                <div class="five columns">
                    <?php

echo ($_SESSION['msg user']);
                        if(empty($_SESSION['valid user']))
                        {
                            echo "Вы вошли на сайт как гость";
                        }
                        else 
                        {
                            echo "Вы вошли как ".$_SESSION['valid user']."<br/>";
                            if ($_SESSION['msg user'])
                            {
                                echo ($_SESSION['msg user']);
                                $_SESSION['msg user'] = '';
                            }
                        }
                    ?>
               
                </div>
            </div>
        </div>
    </body>
</html>