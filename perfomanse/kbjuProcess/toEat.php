<?php

session_start();


?>

<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/skeleton/normalize.css" rel="stylesheet">
        <link href="../css/skeleton/skeleton.css" rel="stylesheet">
        <title>Главная</title>
    </head>
    <body>
        <div class="container">
            
            <div class="row">
                
                <div class="five columns">
                    
                    <h3>Новый прием пищи</h3>
                    <form action="../../logic/kbjuProcess/toEatProcess.php" method="post">

                    <p>
                        <label>Название продукта:</label>
                        <input type="text" name="nameFood">
                    </p>
                    
                    <p>
                        <label>Вес продукта:</label>
                        <input type="text" name="massFood" value="0">
                    </p>

                    <input type="submit">
                    </form>
                </div>
                
            <div class="four columns">
                
                 <h3>Продукты в базе</h3>
                 
                <?php

                //require '/var/www/u0860180/data/dbOn.php';        
                  require 'dbOn.php';  

                if (mysqli_connect_errno())
                {
                    echo 'Не удалось установить соединение';
                    exit;
                }

                $query = "SET NAMES utf8"; // привожу данные к читабельному виду (без этого в базе крокозябры)
                $db->query($query);

                $query = "SELECT foodName from foodarr";

                $result = $db->query($query);
                $num_results = $result->num_rows;
                $array_data = $result->fetch_all();

                for ($i = 0; $i < $num_results; $i++)
                {
                    while ($m = each($array_data[$i]))
                    {

                        echo ($m["value"]);
                        echo '<br/>';
                    }
                }

                ?>
            </div>
             <div class="three columns">
                    
                    <h3>Дальше</h3>
                    <ul class="nav">

                        <li><a href="../../index.php">Главная</a></li>
                        <li><a href="newFood.php">Новый продукт</a></li>
                        <li><a href="../statistic/statistic.php">Статистика</a></li>
                        <li><a href="../userInterface/logout.php">Выход</a></li>
                        <li><a href="../userInterface/reg.php">Регистрация</a></li>
                        <li><a href="../userInterface/writeMe.php">Написать разработчику</a></li>

                    </ul>
                    
                </div>
            </div>
            <div class="row">
                <div class="five columns">   
        <?php
        
                
            if(empty($_SESSION['valid user']))
            {
                echo "Вы вошли на сайт как гость";
            }
            else 
            {
                echo "Вы вошли как ".$_SESSION['valid user']."<br/>";
                            if ( $_SESSION['msg user'])
                            {
                                echo ($_SESSION['msg user']);
                                $_SESSION['msg user'] = '';
                            }
            }
        ?>
               
                </div>
        </div>
        </div>
        
        
    </body>
</html>
