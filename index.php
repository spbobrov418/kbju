<?php
        session_start();
        
        ?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<link href="./perfomanse/css/skeleton/normalize.css" rel="stylesheet">-->
        <!--<link href="./perfomanse/css/skeleton/skeleton.css" rel="stylesheet">-->
        <link href="./perfomanse/css/bootstrap/bootstrap.min.css" rel="stylesheet">
        <title>Главная</title>
    </head>
    <body>
        <div class="container">
            
            <div class="row">
                
                <div class="col-sm-4">
                    
                    <h3>КБЖУ</h3>
                    <script src="./perfomanse/js/jquery.js"></script>
                    <script src="./perfomanse/js/bootstrap.min.js"></script>
                    
                    <form action="logic/userInterface/testreg.php" method="post">
                        <p>
                            <label>Логин:</label>
                            <input name="userName" type="text" size="15" maxlength="15">
                        </p>
                        <p>
                            <label>Пароль:</label>
                            <input name="userPassword" type="password" size="15" maxlength="15">
                        </p>

                        <p>
                            <input type="submit" name="submit" value="Вход">
                        </p>
                    </form>
                    
                </div>
                
                <div class="col-sm-4">
                    
                    <h3>Начало</h3>
                    <ul class="nav">

                        <li><a href="perfomanse/userInterface/reg.php">Регистрация</a></li>
                        
                        <li><a href="perfomanse/userInterface/writeMe.php">Написать разработчику</a></li>

                    </ul>
                    
                </div>
        
               <div class="col-sm-4">
                    
                    <h3>Пользователям</h3>
                    <ul class="nav">

                        <li><a href="perfomanse/kbjuProcess/toEat.php">Прием пищи</a></li>
                        <li><a href="perfomanse/kbjuProcess/newFood.php">Новый продукт</a></li>
                        <li><a href="perfomanse/statistic/statistic.php">Статистика</a></li>
                        <li><a href="perfomanse/userInterface/logout.php">Выход</a></li>

                    </ul>
                    
                </div>
            </div>
            <div class="row">
                <div class="five columns">
        <?php
        
                
            if(empty($_SESSION['valid user']))
            {
                echo "Вы вошли на сайт как гость";
            }
            else 
            {
                echo "Вы вошли как ".$_SESSION['valid user'];
            }
            
            $_SESSION['msg user'] = '';
        ?>
                </div>
        </div>
        </div>
    </body>
</html>
